package com.cts.project.OnlineShopping;

import com.cts.project.model.Product;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppInit {
	@Bean
	public Product getProdut() {
		return new Product(1001,"Amul Milkshake",25.00);
	}
}
