package com.cts.project.OnlineShopping;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.cts.project.model.Product;
import com.cts.project.model.Product2;

/*
 * Annotation
 */
public class App2 
{
    public static void main( String[] args )
    {
    	/*
    	 * Scan the class and get the bean
    	 */
    	ApplicationContext ctx = new AnnotationConfigApplicationContext(AppInit2.class);
    	Product2 product = ctx.getBean(Product2.class);
    	System.out.println(product);
    }
}
