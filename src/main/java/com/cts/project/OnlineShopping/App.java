package com.cts.project.OnlineShopping;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.cts.project.model.Product;

/*
 * Annotation
 */
public class App 
{
    public static void main( String[] args )
    {
    	ApplicationContext ctx = new AnnotationConfigApplicationContext(AppInit.class);
    	Product product = ctx.getBean(Product.class);
    	System.out.println(product);
    }
}
