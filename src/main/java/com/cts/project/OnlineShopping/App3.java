package com.cts.project.OnlineShopping;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.cts.project.model.Product;
import com.cts.project.model.Product2;
import com.cts.project.model.Product3;

/*
 * Annotation
 */
public class App3 
{
    public static void main( String[] args )
    {
    	/*
    	 * Scan the class and get the bean
    	 */
    	ApplicationContext ctx = new AnnotationConfigApplicationContext(AppInit3.class);
    	Product3 product = ctx.getBean(Product3.class);
    	System.out.println(product);
    }
}
