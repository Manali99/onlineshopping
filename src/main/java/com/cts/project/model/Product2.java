package com.cts.project.model;

import org.springframework.stereotype.Component;

@Component
public class Product2 {
	private int produtId;
	private String productName;
	private double productPrice;
	
	public Product2(){
	}
	
	public Product2(int produtId, String productName, double productPrice) {
		super();
		this.produtId = produtId;
		this.productName = productName;
		this.productPrice = productPrice;
	}

	@Override
	public String toString() {
		return "Product [produtId=" + produtId + ", productName=" + productName + ", productPrice=" + productPrice
				+ "]";
	}
	
}
