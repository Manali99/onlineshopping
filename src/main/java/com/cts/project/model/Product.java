package com.cts.project.model;

public class Product {
	private int produtId;
	private String productName;
	private double productPrice;
	
	public Product(){
	}
	
	public Product(int produtId, String productName, double productPrice) {
		super();
		this.produtId = produtId;
		this.productName = productName;
		this.productPrice = productPrice;
	}

	@Override
	public String toString() {
		return "Product [produtId=" + produtId + ", productName=" + productName + ", productPrice=" + productPrice
				+ "]";
	}
	
}
