package com.cts.project.model;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

@Component
@PropertySource("classpath:Product.properties")
public class Product3 {
	
	@Value("${pr.id}")
	private int produtId;
	
	@Value("${pr.name}")
	private String productName;
	
	@Value("${pr.price}")
	private double productPrice;
	
	public Product3(){
	}
	
	public Product3(int produtId, String productName, double productPrice) {
		super();
		this.produtId = produtId;
		this.productName = productName;
		this.productPrice = productPrice;
	}
	
	public int getProdutId() {
		return produtId;
	}

	public void setProdutId(int produtId) {
		this.produtId = produtId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	@Override
	public String toString() {
		return "Product [produtId=" + produtId + ", productName=" + productName + ", productPrice=" + productPrice
				+ "]";
	}
	
}
